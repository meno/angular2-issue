import 'package:angular2/angular2.dart';
import 'package:angular2/platform/browser.dart';

void main() {
  bootstrap(MainApp);
}

@Component(
  selector: 'main-app',
  template: r'''
<el-icon [icon]="'small-' + type"></el-icon>

<div>This should display: SWITCH WORKING</div>
  ''',
  directives: const [IconComponent],
)
class MainApp {
  String type = 'facebook';
}

@Component(
  selector: 'el-icon',
  template: r'''
<span>{{icon}}</span>

<div *ngIf="icon == 'small-facebook'">IF WORKING</div>

<div [ngSwitch]="icon">
  <div *ngSwitchCase="'small-facebook'">SWITCH WORKING</div>
</div>
  ''',
)
class IconComponent {
  @Input()
  String icon;
}
